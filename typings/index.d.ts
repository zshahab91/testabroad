

declare namespace JSX {
	interface IntrinsicElements {
		[elemName: string]: any;
	}
}

declare interface ReduxAction<PayloadType = string> {
	type: string;
	payload: PayloadType;
}
