declare interface BaseDto {
	id?: number;
	createdDateTime?: string;
	updatedDateTime?: string;
}
declare interface BaseLauncheDto {
	flight_number?: number,
	mission_name?: string,
	mission_id?: any[],
	upcoming?: boolean,
	launch_year?: string,
	launch_date_unix?: number,
	launch_date_utc?: Date,
	launch_date_local?: Date,
	is_tentative?: boolean,
	tentative_max_precision?: string,
	tbd?: boolean,
	launch_window?: number,
	rocket?: BaseRocket,
	ships?: any[],
	telemetry?: BaseTelemetry,
	launch_site?: BaseLaunchSite,
	launch_success?: boolean,
	launch_failure_details?: BaseLauncheFailureDetail,
	links?: BaseLink,
	details?: string,
	static_fire_date_utc?: Date,
	static_fire_date_unix?: number,
	timeline?: BaseTimeLine,
	crew?: null
}

declare interface BaseRocket {
	rocket_id?: string,
	rocket_name?: string,
	rocket_type?: string,
	first_stage?: BaseStage,
	second_stage?: BaseSecondStage,
	fairings?: BaseFairing,

}
declare interface BaseStage {
	cores?: BaseCore[]
}
declare interface BaseCore {
	core_serial?: string,
	flight?: number,
	block?: null,
	gridfins?: boolean,
	legs: boolean,
	reused?: boolean,
	land_success?: null,
	landing_intent?: boolean,
	landing_type?: null,
	landing_vehicle?: null
		
}

declare interface BaseSecondStage {
		block?: number,
		payloads?: BasePayload[]
}
declare interface BasePayload {
	
		payload_id?: string,
		norad_id?: any[],
		reused: boolean,
		customers?:string[],
		nationality?: string,
		manufacturer?: string,
		payload_type?: string,
		payload_mass_kg?: number,
		payload_mass_lbs?: number,
		orbit?: string,
		orbit_params?: BaseOrbitParam,
		uid?: string
		
}
declare interface BaseOrbitParam {
	reference_system?: string,
	regime?: string,
	longitude?: null,
	semi_major_axis_km?: null,
	eccentricity?: null,
	periapsis_km?: number,
	apoapsis_km?: number,
	inclination_deg?: number,
	period_min?: null,
	lifespan_years?: null,
	epoch?: null,
	mean_motion?: null,
	raan?: null,
	arg_of_pericenter?: null,
	mean_anomaly?: null
}
declare interface BaseFairing{
	reused?: boolean,
	recovery_attempt?: boolean,
	recovered?: boolean,
	ship?: null
}
declare interface BaseTelemetry {
	flight_club?: null		
}
declare interface BaseLaunchSite {
	site_id?: string,
	site_name?: string,
	site_name_long?: string
}
declare interface BaseLauncheFailureDetail {
	time?: number,
	altitude?: null,
	reason?: string
}
declare interface BaseLink {
	mission_patch?: string,
	mission_patch_small?: string,
	reddit_campaign?: null,
	reddit_launch?: null,
	reddit_recovery?: null,
	reddit_media?: null,
	presskit?: null,
	article_link?: string,
	wikipedia?: string,
	video_link?: string,
	youtube_id?: string,
	flickr_images?: any[]
}
declare interface BaseTimeLine {
	webcast_liftoff?: number
}



