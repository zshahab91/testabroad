
import axios from 'axios'
import { configLocalStorage } from "../../helper/common";



let token = null;
axios.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'}
axios.defaults.baseURL =  "https://api.spacexdata.com/v3/"

const client = (token:any) => {
	// token = localStorage.getItem('token') ? configLocalStorage('get','token') : null;
    // console.log("TCL: client -> token", token)

  const defaultOptions = {
    headers: {
      Authorization: token ? `Token ${token}` : '',
    },
  };

  return {

    get: (url:any) => axios.get(url),
    post: (url:any, data: any, options = {}) => axios.post(url, data, { ...defaultOptions, ...options }),
    put: (url:any, data: any, options = {}) => axios.put(url, data, { ...defaultOptions, ...options }),
    delete: (url:any, options = {}) => axios.delete(url, { ...defaultOptions, ...options }),
  };
};


export  const HTTP = client(token);
