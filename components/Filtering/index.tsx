



import { useDispatch } from "react-redux";
import { useState } from "react";
import { Dropdown } from 'semantic-ui-react'
import * as launchesActions from "../../store/launches/launches.actions";

const Filtering: React.FC = props => {
  const dispatch = useDispatch();

  const stateOptions = [
      {
        key: false,
        text: "Past",
        value: false,
      },
      {
        key: true,
        text: "Upcoming",
        value: true,
      },
      {
        key: "all",
        text: "All",
        value: "all",
      }
  ]
  const [ type , setType ]= useState(false)

 const handleChange = (e:any, obj:any) => {
     setType(obj.value);
     dispatch(launchesActions.launcheFilterRequest(obj.value));
    }


  return (
    <div className="filtering">
        <Dropdown placeholder='All' search  closeOnChange selection options={stateOptions}  onChange={(e, obj) => handleChange(e, obj)}/>
    </div>
  );
};

export default Filtering;
