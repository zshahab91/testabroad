import { useDispatch, useSelector } from "react-redux";
import { Pagination } from 'semantic-ui-react'
import * as launchesActions from "../../store/launches/launches.actions";
import { ILaunchesState } from "../../store/launches/launches.types";


const PaginationData:React.FC = props => {
  const dispatch = useDispatch();
  const { all, currentPage, perPage,totalData, dataPagination } = useSelector(
    (state: any) => state.launches as ILaunchesState
  );

  const totalPages = all ? Math.ceil(totalData/perPage) : 1;

  const onChange = (e:any, pageInfo:any) => {
    const obj = {
      currentPage: pageInfo.activePage,
      dataPag: dataPagination?.length !== 0 ? dataPagination : all
    }
    dispatch(
      launchesActions.launchePaginationRequest(obj)
    );
   
  };


  return (
    <div className="_pagination_holder">
     <Pagination
         boundaryRange={0}
         defaultActivePage={currentPage}
         ellipsisItem={null}
         firstItem={null}
         lastItem={null}
         siblingRange={1}
         totalPages={totalPages}
      onPageChange={onChange}
  />
    </div>
  );
};

export default PaginationData;
