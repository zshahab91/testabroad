import { useDispatch } from "react-redux";
import { useState } from "react";
import { Table, Radio, Icon, Image } from "semantic-ui-react";
import Filtering from "../Filtering";
import PaginationData from "../PaginationData"
import ModalData from "../ModalData";

interface IProps {
  data?: BaseLauncheDto | any;
}
const DataTable: React.FC<IProps> = props => {
  const { data } = props;
  const [openFlag, setOpenFlag]= useState(false);
  const[detailData, setDetailData] = useState(null);

  return (
    <div className="container">
      <Filtering />
      <Table striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Flight Number</Table.HeaderCell>
            <Table.HeaderCell>Img</Table.HeaderCell>
            <Table.HeaderCell>Mission Name</Table.HeaderCell>
            <Table.HeaderCell>UpComing</Table.HeaderCell>
            <Table.HeaderCell>Launch Year</Table.HeaderCell>
            <Table.HeaderCell>Launch Date Unix</Table.HeaderCell>
            <Table.HeaderCell>Launch Window</Table.HeaderCell>
            <Table.HeaderCell>Details</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data &&
            data.map((item: any, inx: any) => {
              return (
                <Table.Row key={inx}>
                  <Table.Cell>{item.flight_number}</Table.Cell>
                  <Table.Cell>
                    <Image
                      src={item.links.mission_patch_small}
                      size="mini"
                      circular
                    />
                  </Table.Cell>
                  <Table.Cell>{item.mission_name}</Table.Cell>
                  <Table.Cell>
                    <Radio checked={item.upcoming} />
                  </Table.Cell>
                  <Table.Cell>{item.launch_year}</Table.Cell>
                  <Table.Cell>{item.launch_date_unix}</Table.Cell>
                  <Table.Cell>{item.launch_window}</Table.Cell>
                  <Table.Cell>
                    <a onClick={() => {setDetailData(item); setOpenFlag(true);}}>
                      <Icon name="eye" />
                    </a>
                  </Table.Cell>
                </Table.Row>
              );
            })}
        </Table.Body>
      </Table>
      <PaginationData />  
      <ModalData openFlag={openFlag} dataModal={detailData &&  detailData} onCloseModal ={() => setOpenFlag(false)}/>    
    </div>
  );
};

export default DataTable;
