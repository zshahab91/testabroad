import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { Button, Header, Image, Modal, Item } from "semantic-ui-react";
import * as launchesActions from "../../store/launches/launches.actions";
import { ILaunchesState } from "../../store/launches/launches.types";

interface IProps {
  dataModal: any;
  openFlag: boolean;
  onCloseModal(): void;
}

const ModalData: React.FC<IProps> = props => {
  const { dataModal } = props;

  return (
    dataModal && (
      <div className="modal">
          <Modal open={props.openFlag} size={'small'}  onClose={() => props.onCloseModal()}
          closeIcon>
     <Modal.Header>Details Of {dataModal.mission_name} launche</Modal.Header>
    <Modal.Content image>
      <Image wrapped size='medium' src={dataModal.links.mission_patch} />
      <Modal.Description>
      <Header>{dataModal.launch_site.site_name}</Header>
              <p>
               {dataModal.launch_site.site_name_long}
              </p>
              <p>{dataModal.details}</p>
              {dataModal.links.video_link &&
                <span>
                  <h4>Youtube link: </h4>
              <a href={dataModal.links.video_link !== null ? dataModal.links.video_link : "https://carolinadojo.com/wp-content/uploads/2017/04/default-image.jpg"} target="_blank">{dataModal.links.video_link}</a>
                </span>
              }
      </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
              <Button primary >
              <a style={{color: "#fff"}} href={dataModal.links.article_link!== null ? dataModal.links.article_link : dataModal.links.reddit_campaign} target="_blank">Read More</a>
                </Button>
                <Button negative onClick={() => props.onCloseModal()}>
                  Close
                </Button>
              </Modal.Actions>
      </Modal>

      </div>
    )
  );
};

export default ModalData;
