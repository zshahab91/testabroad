import React, { useEffect } from "react";
import makeStyles from "./styles";
import { useDispatch, useSelector } from "react-redux";

import { ILaunchesState } from "../../store/launches/launches.types";
import * as launchesActions from "../../store/launches/launches.actions";

import DataTable from "../../components/DataTable";

const HomeScreen: React.FC = (props) => {
  const classes = makeStyles(props);
  const dispatch = useDispatch();
  const { all, filters, currentPage, perPage } = useSelector(
    (state: any) => state.launches as ILaunchesState
  );
  useEffect(() => {
    dispatch(launchesActions.launcheGetListAllRequest());
  }, []);
  const dataOfPage = filters?.length === 0 ? all && all.slice(0, currentPage * perPage) : filters && filters.slice(0, currentPage * perPage);
  return (
    <div className="container-fluid"> 
      <DataTable data={dataOfPage }/>
    </div>
  );
};

export default HomeScreen;
