# Test Zahra Shahab

In this project uses from:

  - create-react-app
  - next.js
  - typescript
  - redux-saga
  - react.semantic-ui
  - axios

### Installation

Dillinger requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd TestAbroad
$ yarn OR npm install
```

For production environments...

```sh
$ yarn build
```




### Development

Want to contribute? Great!

Dillinger uses Gulp + Webpack for fast developing.
Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.

First Tab:
```sh
$ yarn dev OR npm run dev
```

Second Tab:
```sh
$ open your browser http://localhost:3000/
```
