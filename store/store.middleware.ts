import createSagaMiddleware from "redux-saga";
import { applyMiddleware, compose } from "redux";

import { Middleware, StoreEnhancer } from "redux";

export const sagaMiddleware = createSagaMiddleware();

// @ts-ignore
// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const createRootMiddleWare = (): StoreEnhancer => {
	const middleware: Middleware[] = [sagaMiddleware];


	// Middlewares only active on development mode
	if (process.env.NODE_ENV === "development") {
		const { createLogger } = require("redux-logger");
		middleware.push(createLogger({ collapsed: true }));

		const invariant = require("redux-immutable-state-invariant").default;
		middleware.push(invariant());
	}
	if (process.env.NODE_ENV !== "production") {
		const { composeWithDevTools } = require("redux-devtools-extension");
		// arrMiddleware.push(createLogger());
		return composeWithDevTools(applyMiddleware(...middleware));
	  }

	// return composeEnhancer(applyMiddleware(...middleware));
	return applyMiddleware(...middleware);

};

export default createRootMiddleWare;
