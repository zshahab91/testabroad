import { all, fork } from "redux-saga/effects";
// sagas entities


import LaunchesSagas from "./launches/launches.sagas";


/**
 * rootSaga
 */
export default function* root() {
	yield all([
		fork(LaunchesSagas),

	]);
}
