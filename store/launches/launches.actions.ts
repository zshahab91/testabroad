/**
 * @module Actions/Grid
 * @desc Grid Actions
 */
import { createActions } from "redux-actions";
import { ActionTypes } from "./launches.constants";

export const {
	launcheGetListAllRequest,
	launcheGetListAllSuccess,
	launcheGetListAllFailure,

	launcheFilterRequest,
	launchePaginationRequest


} = createActions({
	[ActionTypes.LAUNCHE_GET_LIST_ALL_REQUEST]: (payload: any) => payload,
	[ActionTypes.LAUNCHE_GET_LIST_ALL_SUCCESS]: (payload: any) => payload,
	[ActionTypes.LAUNCHE_GET_LIST_ALL_FAILURE]: (payload: any) => payload,

	[ActionTypes.LAUNCHE_FILTER_REQUEST]: (payload: any) => payload,
	[ActionTypes.LAUNCHE_PAGINATION_REQUEST]: (payload: any) => payload,




});
