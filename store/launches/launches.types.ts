
export interface ILaunchesState{
	message?: string,
	all?: BaseLauncheDto[]  ,
	isLoading ?: boolean,
	filters ?: BaseLauncheDto[] ,
	currentPage: number,
	perPage: number,
	totalData: number,
	dataPagination?: BaseLauncheDto[]
}


export interface ActionTypes {
	LAUNCHE_GET_LIST_ALL_REQUEST: string;
	LAUNCHE_GET_LIST_ALL_SUCCESS: string;
	LAUNCHE_GET_LIST_ALL_FAILURE: string;


	LAUNCHE_FILTER_REQUEST: string;
	LAUNCHE_PAGINATION_REQUEST: string;


	
}
