import keyMirror from "keymirror";
import { ActionTypes as Types } from "./launches.types";


export const ActionTypes: Types = keyMirror({
	LAUNCHE_GET_LIST_ALL_REQUEST: undefined,
	LAUNCHE_GET_LIST_ALL_SUCCESS: undefined,
	LAUNCHE_GET_LIST_ALL_FAILURE: undefined,


	LAUNCHE_FILTER_REQUEST: undefined,
	LAUNCHE_PAGINATION_REQUEST: undefined,



});
