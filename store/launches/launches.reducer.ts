import { handleActions } from "redux-actions";
import produce, { Draft } from "immer";
import { ActionTypes } from "./launches.constants";
import { ILaunchesState } from "./launches.types";

const initialState: ILaunchesState = {
	message: "",
	all: [],
	filters: [],
	isLoading: false,
	currentPage: 1,
	perPage: 10,
	totalData: 0,
	dataPagination: []
	

};

export default handleActions(
	{
		[ActionTypes.LAUNCHE_GET_LIST_ALL_REQUEST]: (state, { payload }: any) =>
			produce(state, (draft: Draft<ILaunchesState>) => {
				draft.isLoading = true;
				draft.message = "";
			}),
		[ActionTypes.LAUNCHE_GET_LIST_ALL_SUCCESS]: (state, { payload }: any) =>
		
		produce(state, (draft: Draft<ILaunchesState>) => {				
				draft.all = payload;
				draft.isLoading = false;
				draft.totalData = payload.length;
			}),
		[ActionTypes.LAUNCHE_GET_LIST_ALL_FAILURE]: (state, { payload }: any) =>
			produce(state, (draft: Draft<ILaunchesState>) => {
				draft.message = payload.message;
				draft.isLoading = false;
			}),


		[ActionTypes.LAUNCHE_FILTER_REQUEST]: (state, { payload }: any) =>
			produce(state, (draft: Draft<ILaunchesState>) => {
				draft.filters = [];
				draft.filters = payload !== "all" ? draft.all?.filter((item:any) => item.upcoming === payload) : draft.all;
				// @ts-ignore
				draft.totalData = draft.filters ? draft.filters?.length : draft.all?.length ;
				draft.dataPagination = draft.filters;
			}),

		[ActionTypes.LAUNCHE_PAGINATION_REQUEST]: (state, { payload }: any) =>
			produce(state, (draft: Draft<ILaunchesState>) => {
				const { currentPage, dataPag } = payload;
				draft.currentPage = currentPage;
				draft.filters = currentPage > 1  ? dataPag.slice((currentPage - 1) * draft.perPage, currentPage * draft.perPage) : dataPag.slice(0, currentPage * draft.perPage);
			
			}),		


	
	},
	initialState,
);
