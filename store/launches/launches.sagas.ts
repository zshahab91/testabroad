/**
 * @module Sagas/Area
 * @desc Common Area
 */
import { all, put, takeLatest, select } from "redux-saga/effects";
import { ActionTypes } from "./launches.constants";
import { GetListLaunchesAll } from "../../services/http/endpoints/common";



function* launchesGetListAllSaga() {

	try {
		const response = yield GetListLaunchesAll('launches').then((res:any) => res.data);
		yield put({
			type: ActionTypes.LAUNCHE_GET_LIST_ALL_SUCCESS,
			payload: response
		});

	} catch (error) {
		yield put({
			type: ActionTypes.LAUNCHE_GET_LIST_ALL_FAILURE,
			payload: null,
		});
	}
}


export default function* root() {
	yield all([
		takeLatest(ActionTypes.LAUNCHE_GET_LIST_ALL_REQUEST, launchesGetListAllSaga),

	]);
}
