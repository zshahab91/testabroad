


import { combineReducers } from 'redux';

import Launches_Reducer from "./launches/launches.reducer";


const rootReducer = combineReducers({

  launches: Launches_Reducer,

  // add other reducers to the array
});
export default rootReducer;