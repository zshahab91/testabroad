import Router from 'next/router'
import nextCookie from 'next-cookies'


export const auth = (ctx: any) => {
  const { token } = nextCookie(ctx);

  // If there's no token, it means the user is not logged in.
  if (!token) {
    if (typeof window === 'undefined') {
      // ctx.res.writeHead(302, { Location: '/login' })
      // ctx.res.end()
    } else {
      // Router.push('/login')
    }
  }

  return token
}


export const validateType = (val: any) => {
  let patternEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let patternPhone = /^[1-9]\d{9}$/g;
  if (val.includes(0)) {
    //@ts-ignore
    return patternPhone.test(parseInt(val));
  } else {
    return patternEmail.test(String(val).toLowerCase());
  }
};
export const configLocalStorage = (type: string, key: string, value?: any) => {
  if (localStorage) {
    switch (type) {
      case "set":
        localStorage.setItem(key, value);
        break;
      case "get":
        return localStorage.getItem(key);
      case "remove":
        localStorage.removeItem(key);
        break;
      case "clear":
        localStorage.clear();
      default:
        return null;
    }
  }
};

export const sortData = (data: any, key: string, flag: string) => {
  if (flag === "sort") {
    return data.sort((a: any, b: any) => {
      const x = a[key];
      const y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  } else {
    return data.reverse((a: any, b: any) => {
      const x = a[key];
      const y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
};

export const filterData = (
  data: any,
  from: number,
  to: number,
  key: string
) => {
  return data.filter((item: any) => item[key] > from && item[key] < to);
};

